﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Decrypt.vi" Type="VI" URL="../Decrypt.vi"/>
		<Item Name="Encrypt.vi" Type="VI" URL="../Encrypt.vi"/>
		<Item Name="Key Generator.vi" Type="VI" URL="../Key Generator.vi"/>
		<Item Name="Only_Decode.vi" Type="VI" URL="../Only_Decode.vi"/>
		<Item Name="preob_matrix.vi" Type="VI" URL="../preob_matrix.vi"/>
		<Item Name="reverse.vi" Type="VI" URL="../reverse.vi"/>
		<Item Name="FPGA Target" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{02B2CD04-A611-4583-B1C0-B49EB79AACAE}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW0;0;ReadMethodType=bool{084C2573-88D3-4831-9476-824A5039406B}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED0;0;ReadMethodType=bool;WriteMethodType=bool{1566347E-4DE7-4EDC-AE26-E5BB691F7CE9}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW2;0;ReadMethodType=bool{207898D6-B7B4-4507-9DC8-77491921CBA0}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED3;0;ReadMethodType=bool;WriteMethodType=bool{6800223B-8B78-4BC7-8D22-F6328DD6AE05}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW1;0;ReadMethodType=bool{9D3B88DC-CD22-414F-A8EB-33D5C4DE7E63}ResourceName=OnboardClock;TopSignalConnect=OnboardClock;ClockSignalName=OnboardClock;MinFreq=50000000.000000;MaxFreq=50000000.000000;VariableFreq=0;NomFreq=50000000.000000;PeakPeriodJitter=300.000000;MinDutyCycle=40.000000;MaxDutyCycle=60.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B007D335-CFCA-4DAD-868F-B1DEAA8B25F0}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED2;0;ReadMethodType=bool;WriteMethodType=bool{B64B0668-6C35-4E22-BB27-0CD87B6A4CF6}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW3;0;ReadMethodType=bool{DDFFF238-629D-47EA-A437-EBABA714CC89}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED1;0;ReadMethodType=bool;WriteMethodType=boolDE FPGA Board/OnboardClock/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSDE_FPGA_BOARDFPGA_TARGET_FAMILYSPARTAN3ETARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">DE FPGA Board/OnboardClock/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSDE_FPGA_BOARDFPGA_TARGET_FAMILYSPARTAN3ETARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]LED0ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED0;0;ReadMethodType=bool;WriteMethodType=boolLED1ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED1;0;ReadMethodType=bool;WriteMethodType=boolLED2ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED2;0;ReadMethodType=bool;WriteMethodType=boolLED3ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED3;0;ReadMethodType=bool;WriteMethodType=boolOnboardClockResourceName=OnboardClock;TopSignalConnect=OnboardClock;ClockSignalName=OnboardClock;MinFreq=50000000.000000;MaxFreq=50000000.000000;VariableFreq=0;NomFreq=50000000.000000;PeakPeriodJitter=300.000000;MinDutyCycle=40.000000;MaxDutyCycle=60.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;SW0NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW0;0;ReadMethodType=boolSW1NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW1;0;ReadMethodType=boolSW2NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW2;0;ReadMethodType=boolSW3NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW3;0;ReadMethodType=bool</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">DE FPGA Board/OnboardClock/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSDE_FPGA_BOARDFPGA_TARGET_FAMILYSPARTAN3ETARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">Dev1</Property>
			<Property Name="Target Class" Type="Str">DE FPGA Board</Property>
			<Property Name="Top-Level Timing Source" Type="Str">OnboardClock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Slide Switches" Type="Folder">
				<Item Name="SW0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Slide Switches/SW0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{02B2CD04-A611-4583-B1C0-B49EB79AACAE}</Property>
				</Item>
				<Item Name="SW1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Slide Switches/SW1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6800223B-8B78-4BC7-8D22-F6328DD6AE05}</Property>
				</Item>
				<Item Name="SW2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Slide Switches/SW2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1566347E-4DE7-4EDC-AE26-E5BB691F7CE9}</Property>
				</Item>
				<Item Name="SW3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Slide Switches/SW3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B64B0668-6C35-4E22-BB27-0CD87B6A4CF6}</Property>
				</Item>
			</Item>
			<Item Name="LEDs" Type="Folder">
				<Item Name="LED0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>ArbitrateIfMultipleAccessorsOnly</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/LEDs/LED0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{084C2573-88D3-4831-9476-824A5039406B}</Property>
				</Item>
				<Item Name="LED1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>ArbitrateIfMultipleAccessorsOnly</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/LEDs/LED1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DDFFF238-629D-47EA-A437-EBABA714CC89}</Property>
				</Item>
				<Item Name="LED2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>ArbitrateIfMultipleAccessorsOnly</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/LEDs/LED2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B007D335-CFCA-4DAD-868F-B1DEAA8B25F0}</Property>
				</Item>
				<Item Name="LED3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>ArbitrateIfMultipleAccessorsOnly</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/LEDs/LED3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{207898D6-B7B4-4507-9DC8-77491921CBA0}</Property>
				</Item>
			</Item>
			<Item Name="OnboardClock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{9D3B88DC-CD22-414F-A8EB-33D5C4DE7E63}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=OnboardClock;TopSignalConnect=OnboardClock;ClockSignalName=OnboardClock;MinFreq=50000000.000000;MaxFreq=50000000.000000;VariableFreq=0;NomFreq=50000000.000000;PeakPeriodJitter=300.000000;MinDutyCycle=40.000000;MaxDutyCycle=60.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">OnboardClock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">60</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">50000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">50000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">50000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">300</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">OnboardClock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">OnboardClock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="RM.vi" Type="VI" URL="../RM.vi">
				<Property Name="configString.guid" Type="Str">{02B2CD04-A611-4583-B1C0-B49EB79AACAE}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW0;0;ReadMethodType=bool{084C2573-88D3-4831-9476-824A5039406B}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED0;0;ReadMethodType=bool;WriteMethodType=bool{1566347E-4DE7-4EDC-AE26-E5BB691F7CE9}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW2;0;ReadMethodType=bool{207898D6-B7B4-4507-9DC8-77491921CBA0}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED3;0;ReadMethodType=bool;WriteMethodType=bool{6800223B-8B78-4BC7-8D22-F6328DD6AE05}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW1;0;ReadMethodType=bool{9D3B88DC-CD22-414F-A8EB-33D5C4DE7E63}ResourceName=OnboardClock;TopSignalConnect=OnboardClock;ClockSignalName=OnboardClock;MinFreq=50000000.000000;MaxFreq=50000000.000000;VariableFreq=0;NomFreq=50000000.000000;PeakPeriodJitter=300.000000;MinDutyCycle=40.000000;MaxDutyCycle=60.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B007D335-CFCA-4DAD-868F-B1DEAA8B25F0}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED2;0;ReadMethodType=bool;WriteMethodType=bool{B64B0668-6C35-4E22-BB27-0CD87B6A4CF6}NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW3;0;ReadMethodType=bool{DDFFF238-629D-47EA-A437-EBABA714CC89}ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED1;0;ReadMethodType=bool;WriteMethodType=boolDE FPGA Board/OnboardClock/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSDE_FPGA_BOARDFPGA_TARGET_FAMILYSPARTAN3ETARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">DE FPGA Board/OnboardClock/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSDE_FPGA_BOARDFPGA_TARGET_FAMILYSPARTAN3ETARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]LED0ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED0;0;ReadMethodType=bool;WriteMethodType=boolLED1ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED1;0;ReadMethodType=bool;WriteMethodType=boolLED2ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED2;0;ReadMethodType=bool;WriteMethodType=boolLED3ArbitrationForOutputData=ArbitrateIfMultipleAccessorsOnly;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/LEDs/LED3;0;ReadMethodType=bool;WriteMethodType=boolOnboardClockResourceName=OnboardClock;TopSignalConnect=OnboardClock;ClockSignalName=OnboardClock;MinFreq=50000000.000000;MaxFreq=50000000.000000;VariableFreq=0;NomFreq=50000000.000000;PeakPeriodJitter=300.000000;MinDutyCycle=40.000000;MaxDutyCycle=60.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;SW0NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW0;0;ReadMethodType=boolSW1NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW1;0;ReadMethodType=boolSW2NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW2;0;ReadMethodType=boolSW3NumberOfSyncRegistersForReadInProject=Auto;resource=/Slide Switches/SW3;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\Users\avr123\Desktop\Conference\FPGA Bitfiles\Conference_FPGATarget_RM_Dn0kroT7gxk.lvbitx</Property>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="RM" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">RM</Property>
					<Property Name="Comp.BitfileName" Type="Str">Conference_FPGATarget_RM_Dn0kroT7gxk.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/Users/avr123/Desktop/Conference/FPGA Bitfiles/Conference_FPGATarget_RM_Dn0kroT7gxk.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/Conference_FPGATarget_RM_Dn0kroT7gxk.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/Users/avr123/Desktop/Conference/Conference.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">FPGA Target</Property>
					<Property Name="TopLevelVI" Type="Ref">/My Computer/FPGA Target/RM.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="vi.lib\LabVIEW Targets\NiCobs\bin\nt\NiCobsLv.dll" Type="Document" URL="vi.lib\LabVIEW Targets\NiCobs\bin\nt\NiCobsLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
